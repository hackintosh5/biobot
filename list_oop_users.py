#    This file is part of BioBot
#    Copyright (C) 2018 Penn Mackintosh
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import userbot_helper
import asyncio
from constants import *

async def test():
    a = ["ManuWillFake","rr98076","arjaan_a_m","Persieghini","sicrs","lockedp","EduBR029","JamesRandy96","Tsutsukakushi","Bannerets","adalaran","haaaah","Orfeu","SovietAmericanCat","Furfi","bubbly1194","ricky1337","RegisTheForgotten","MortyBih","TheSpilledMilk","RetardedCat","electroxexual","AgEzRo","TravisIsNeverGonnaGiveYouUp","asafniv","Sgitkene","reamann","millicow"]
    a += ["nathangreen06","DodoGTA","wyldbot","Nithin_Joseph","kufnir","matrix_1220","rupansh","shanuflash","Becca709","raliovi","dbdtrapper","JvHaile","Yes_I_Am","daerok_bot","Fangorncu","mochin69","Agent_HV","DaSonixier","blank_x","SphericalKat","ArtfulArdvark","AverageHuman","DaHedgehog","das_j","WORLD_WAR_0","cva_thapa","Orgeymaster","nabushika","SmashAsh","MCterra","PJ_zzz","Cityana","Sayan_tan","lazerl0rd","mahi_daniel","kektesh","NeonDragon1909","GloriousBastard","AkshitGarg","OneInAMinion","soermejo","infinitEplus","Nick80835","KhaledSecond","Chronosapien","tychontychon","vvreddy","felurx","xjoey","MyCuntryIsBetterThanYours","Harleysaurusrex","Poolitzer","fetusryro","iLerndGramrGud","MinorDeity","wubbalubbadubrobb","looking_to_buy_friends","RealMinorDeity","basitbinmajeed","amolith","wasserkatze","moeenmehraj","kiritjom","sewerbaby","UpdateWarningErrrorUnavailable49","VKYzz","Bobberak","WorstRequest","Eyras","Kaka_r0t","zappymussel380","DiscipleOfBalance","ojogoperdi","marminino","Kitfo","Synuser","OverCoder","jubeosaurus","gotenksIN"]
    a += ["jiviteshkuk","Spleks","peaktogoo","Lunaresk","phoenixatom","bengris32","loque_athene","Hackintosh5","kc4knc","xieve","Jyrkka","BLOODPOPS","AllenWalker","parth111999","AlphonseElricc","EdwardElricc","ninturez0doggOwO","avocore","Complete_Strager","pikapika_bitch","DaddyDick","Confused_Angel","NoTolerance","rasamrajas","tobias788","vdbhb59","Mr_True","v0idifier","hotgirlcancerpico","newbyte","nehal858","lrruta","Domenica4Marzo","akshat28","alexius_vivo","strangenoob","SonoMozzik","emergent_order","AgentNoReply","invisibazinga","bakaotaku_2002","Iron_Sight","THANOS_X","zgiuly1994","Nico53","aragon12","AndroidPie9","rosyavgconsumer","ayamdobhal","JollyJenkins","smart_geek","fcknretard","simrat39","HuntedWriter","tobsss","likony","OddFella","Krutikjain","EagleKing","R8MyDicc","Zenexer","JSPiRiT1337","pika_chuu","OneraZan","lirzw"]
    a = [x.lower() for x in a]
    x = [y.username.lower() for y in await userbot_helper.list_group(MAIN_ID) if y.username]
    for user in x:
        if not user in a:
            print("in group but not in chain", user)
    for user in a:
        if not user in x:
            print("in chain but not in group", user)

asyncio.get_event_loop().run_until_complete(test())
