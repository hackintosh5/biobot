#    This file is part of BioBot
#    Copyright (C) 2018 Penn Mackintosh
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

MAIN_ID=-1001180504638
ADMISSION_ID=-1001315024264
BOT_ID=-1001222289675
GET_USERNAME=", please get a username before you can join."
PM_BOT=", please PM this bot for instructions to join."
INVITE_LEAK="invite link leak placeholder"
BANNED_USERNAME="banned username placeholder"
DIFF_OUTPUT="The following users have joined the chain:\n{}\nThe following users have left the chain:\n{}\nThe following users have changed their bios:\n{}"
PLEASE_WAIT="Please wait..."
SET_BIO="""Please:
1. read @bio_chain_2_rules carefully and ensure you agree.
2. ensure you have set a username.
3. add  {} to your bio.
4. reply to this message."""
