#    This file is part of BioBot
#    Copyright (C) 2018 Penn Mackintosh
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


def diff(oldchain, newchain):
    print(oldchain)
    oldchain = [x.lower() for x in oldchain]
    oldconns = {}

    newchain = [x.lower() for x in newchain]
    newconns = {}

    x=0
    while x < len(oldchain)-1:
        oldconns[oldchain[x]] = oldchain[x+1]
        x += 1

    x=0
    while x < len(newchain)-1:
        newconns[newchain[x]] = newchain[x+1]
        x += 1

    wrong = []
    gone = []
    added = []

    for x in oldconns.keys():
        if not x in newconns.keys():
            gone += [x]
            print("missing member in newconns", x)


    for x in newconns.keys():
        if not x in oldconns.keys():
            added += [x]
            print("missing member in oldconns", x)

    for x in oldconns.keys():
        if x in newconns.keys():
            if newconns[x] != oldconns[x]:
                cont = True
                xn = x
                print(newconns[x], "should point to", oldconns[x])
                while cont:
                    if oldconns[xn] in gone:
                        xn = oldconns[xn]
                    else:
                        if not oldconns[xn] == newconns[x]:
                            wrong += [x]
                        cont = False
    return [added, gone, wrong]
